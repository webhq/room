package website

import (
	"html/template"
	"net/http"
	"path/filepath"
)

var (
	// ViewDirectory represents the default path to the website's template files
	ViewDirectory = "./src/website/views/"
	templates     *template.Template
)

// Render writes a page to the http response
func Render(w http.ResponseWriter, page string, model interface{}) error {
	if templates == nil {
		// Consume all templates within the specified directory into memory
		templates = template.Must(template.New("").ParseGlob(filepath.Join(ViewDirectory, "*.html")))
	}

	return templates.ExecuteTemplate(w, page, model)
}
