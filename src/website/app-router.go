package website

import (
	"fmt"
	"mime"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/AndyRN/room/src/pin"
	"gitlab.com/AndyRN/room/src/website/models"
)

var (
	// StaticDirectory represents the default path to the website's public files
	StaticDirectory = "./src/website/static/"
)

// AppRouter is responsible for HTTP routing within the project
type AppRouter struct {
	Router chi.Router
}

// NewDefaultAppRouter creates a new app router with default middleware and routes
func NewDefaultAppRouter() *AppRouter {
	a := NewAppRouter()
	a.SetupMiddleware()
	a.SetupRoutes()
	return a
}

// NewAppRouter creates a new app router
func NewAppRouter() *AppRouter {
	return &AppRouter{
		Router: chi.NewRouter(),
	}
}

// SetupMiddleware wires up all default middleware
func (a *AppRouter) SetupMiddleware() {
	a.Router.Use(middleware.RequestID)
	a.Router.Use(middleware.Logger)
	a.Router.Use(middleware.Recoverer)
}

// SetupRoutes wires up all default routes
func (a *AppRouter) SetupRoutes() {
	// Resolve mime types here as the local machine registry is used to determine extension types
	mime.AddExtensionType(".js", "application/javascript")

	// Enable public files to be requested
	a.Router.Handle("/static/*", http.StripPrefix("/static/", http.FileServer(http.Dir(StaticDirectory))))

	// Test site connectivity
	a.Router.Route("/echo", func(r chi.Router) {
		r.Get("/*", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "Echo: %v", r.URL.Path[6:])
		})
	})

	// Return index on root requests
	a.Router.Get("/", index)
}

func index(w http.ResponseWriter, r *http.Request) {
	model := &models.Home{
		Occupied: pin.Occupied,
	}
	Render(w, "index", model)
}
