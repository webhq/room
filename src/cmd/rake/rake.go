package main

import (
	"fmt"
	"os"
	"os/exec"
)

const (
	binDataFilename = "room-bindata.go"
)

func main() {
	fmt.Println("--- Starting RAKE ---")

	fmt.Println("Setting environment variables...")
	setenv("GOOS", "linux")
	setenv("GOARCH", "arm")
	setenv("GOARM", "5")

	fmt.Println("Generating assets...")
	// execCmd("go-bindata", "-o", binDataFilename, "./src/website/...")

	fmt.Println("Building project...")
	execCmd("go", "build", "-v", "gitlab.com/AndyRN/room/src/cmd/room")

	fmt.Println("Cleaning up...")
	// os.Remove(binDataFilename)

	fmt.Println("--- Finished ---")
}

func setenv(name string, val string) {
	err := os.Setenv(name, val)
	panicErr(err)
}

func execCmd(name string, arg ...string) {
	cmd := exec.Command(name, arg...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Start()
	err := cmd.Wait()
	panicErr(err)
}

func panicErr(err error) {
	if err != nil {
		panic(err)
	}
}
